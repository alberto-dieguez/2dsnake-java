
import java.awt.Color;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Window extends JFrame {

	private final int WIDTH = 1048;
	private final int HEIGHT = 860;
	JFrame window = new JFrame();
	Board board = new Board();

	public Window() {
		window.setTitle("Snake Game");
		window.setBounds(0, 0, WIDTH, HEIGHT);
		window.setResizable(false);
		window.setVisible(true);
		window.setBackground(Color.DARK_GRAY);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.add(board);

	}
}
