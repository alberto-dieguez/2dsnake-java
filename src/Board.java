
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Board extends JPanel {
	
	private final Color TEXT_COLOR_HEADER = new Color(33,83,209);
	private final Font FONT_HEADER = new Font ("Consolas", 1, 46);
	public Board() {
		
	}
	public void paint(Graphics g) {
		paintBorderInfo(g);
		paintBackgroundInfo(g);
		paintBorderBoardGame(g);
		paintBackgroundBoardGame(g);
		paintTextHeader(g);	
	}
	public void paintBorderInfo(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawRect(20, 20, 1001, 71);
	}
	public void paintBackgroundInfo(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(21, 21, 1000, 70);
	}
	public void paintBorderBoardGame(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawRect(21, 110, 1001, 701);
	}
	public void paintBackgroundBoardGame(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(22, 111, 1000, 700);
	}
	public void paintTextHeader(Graphics g) {
		g.setFont(FONT_HEADER);
		g.setColor(TEXT_COLOR_HEADER);
		g.drawString("Snake Game", 390, 70);
	}
	
}
